package com.example.dessertsapp

import android.annotation.SuppressLint
import android.content.ContentValues.TAG
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.core.text.set
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.example.dessertsapp.ui.fragments.LoginFragment
import com.example.dessertsapp.ui.fragments.MapFragment
import com.example.dessertsapp.ui.fragments.MapFragmentDirections
import com.example.dessertsapp.ui.viewmodel.MainViewModel
import com.google.android.gms.maps.GoogleMap
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import java.util.jar.Manifest

class MainActivity : AppCompatActivity() {
    lateinit var topAppBar: MaterialToolbar
    lateinit var navigationView: NavigationView
    lateinit var drawerLayout: DrawerLayout
    lateinit var logoutBtn: Button
    lateinit var viewModel: MainViewModel
    lateinit var userEmail: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_DessertsApp)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        //Init vars
        topAppBar = findViewById(R.id.topAppBar)
        navigationView = findViewById(R.id.navigationView)
        drawerLayout = findViewById(R.id.drawerLayout)
        val headerView = navigationView.getHeaderView(0)
        userEmail = headerView.findViewById(R.id.user_email_header)
        logoutBtn = findViewById(R.id.logout)
        viewModel = ViewModelProvider(this)[MainViewModel::class.java]

        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)

        //Navigation Menu button
        topAppBar.setNavigationOnClickListener {
            drawerLayout.open()
        }

        drawerLayout.addDrawerListener(object: DrawerLayout.DrawerListener {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                val user = FirebaseAuth.getInstance().currentUser?.email
                if  (user == null) {
                    userEmail.text = "user_email"
                } else {
                    userEmail.text = user
                }
            }

            override fun onDrawerOpened(drawerView: View) {
                val user = FirebaseAuth.getInstance().currentUser?.email
                userEmail.text = user
            }

            override fun onDrawerClosed(drawerView: View) {
            }

            override fun onDrawerStateChanged(newState: Int) {
            }

        })

        //Navigation Drawer
        navigationView.setNavigationItemSelectedListener { menuItem ->
            // Handle menu item selected
//            menuItem.isChecked = true
            when (menuItem.itemId) {
                R.id.add_marker -> {
                    findNavController(R.id.nav_host_fragment).navigate(R.id.mapFragment)
                    val action = MapFragmentDirections.actionMapFragmentToAddMarkerFragment(
                        viewModel.myLocation.latitude.toFloat(),
                        viewModel.myLocation.longitude.toFloat()
                    )
                    findNavController(R.id.nav_host_fragment).navigate(action)
                }
                R.id.marker_list -> {
                    findNavController(R.id.nav_host_fragment).navigate(R.id.markerListFragment)
                }
                R.id.map -> {
                    findNavController(R.id.nav_host_fragment).navigate(R.id.mapFragment)
                }
            }
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
            drawerLayout.close()
            true
        }

        //Logout button
        logoutBtn.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            findNavController(R.id.nav_host_fragment).navigate(R.id.loginFragment)
            drawerLayout.close()
        }
    }

}