package com.example.dessertsapp.ui.fragments

import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.dessertsapp.R
import com.example.dessertsapp.model.MyMarker
import com.example.dessertsapp.ui.adapters.MarkerAdapter
import com.example.dessertsapp.ui.adapters.SwipeToDelete
import com.example.dessertsapp.ui.adapters.SwipeToDeleteLandscape
import com.example.dessertsapp.ui.viewmodel.MainViewModel
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MarkerListFragment : Fragment(R.layout.fragment_marker_list) {
    private lateinit var topAppBar: MaterialToolbar
    private lateinit var addMarker: FloatingActionButton
    private lateinit var searchView: SearchView
    private val viewModel: MainViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Init variables
        topAppBar = activity!!.findViewById(R.id.topAppBar)
        addMarker = view.findViewById(R.id.marker_list_floating_btn)
        searchView = view.findViewById(R.id.searchView)

        topAppBar.title = "Markers List"

        // Initializing RecyclerView and Adapter
        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        val adapter = MarkerAdapter(viewModel.markerList)

        var orientation: Int = resources.configuration.orientation
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            recyclerView.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
        } else {
            recyclerView.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
        }
        recyclerView.adapter = adapter

        addMarker.setOnClickListener{
            val action = MarkerListFragmentDirections.actionMarkerListFragmentToAddMarkerFragment(viewModel.myLocation.latitude.toFloat(), viewModel.myLocation.longitude.toFloat())
            findNavController().navigate(action)
        }

        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            @RequiresApi(Build.VERSION_CODES.N)
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            @RequiresApi(Build.VERSION_CODES.N)
            override fun onQueryTextChange(newText: String?): Boolean {
                recyclerView.adapter = MarkerAdapter(adapter.markerList.filter { it.name.lowercase().contains(newText!!.lowercase()) } as MutableList<MyMarker>)
                return false
            }
        })

        // Adding SwipeToDelete to ItemTouchHelper
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            val itemTouchHelper = ItemTouchHelper(SwipeToDeleteLandscape(adapter))
            itemTouchHelper.attachToRecyclerView(recyclerView)
        } else {
            val itemTouchHelper = ItemTouchHelper(SwipeToDelete(adapter))
            itemTouchHelper.attachToRecyclerView(recyclerView)
        }
    }
}