package com.example.dessertsapp.ui.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.dessertsapp.R
import com.example.dessertsapp.model.MyMarker
import com.example.dessertsapp.ui.viewmodel.MainViewModel
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.floatingactionbutton.FloatingActionButton
import android.graphics.Bitmap
import android.graphics.Canvas

import com.google.android.gms.maps.model.*


class MapFragment : Fragment(), OnMapReadyCallback {

    companion object {
        fun newInstance() = MapFragment()
        const val REQUEST_CODE_LOCATION = 100
    }

    private val viewModel: MainViewModel by activityViewModels()
    private lateinit var map: GoogleMap
    lateinit var topAppBar: MaterialToolbar
    lateinit var addMarker: FloatingActionButton

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Init variables
        topAppBar = activity!!.findViewById(R.id.topAppBar)
        addMarker = view.findViewById(R.id.map_floating_btn)

        topAppBar.visibility = View.VISIBLE
        topAppBar.title = "Map"

        addMarker.setOnClickListener {
            if (map.myLocation != null && map.isMyLocationEnabled) {
                val action = MapFragmentDirections.actionMapFragmentToAddMarkerFragment(
                    map.myLocation.latitude.toFloat(),
                    map.myLocation.longitude.toFloat()
                )
                findNavController().navigate(action)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // TODO: Use the ViewModel
        createMap()
    }

    fun createMap() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        val nightModeFlags = context!!.resources.configuration.uiMode and
                Configuration.UI_MODE_NIGHT_MASK
        when (nightModeFlags) {
            Configuration.UI_MODE_NIGHT_YES ->
                googleMap.setMapStyle(context?.let {
                    MapStyleOptions.loadRawResourceStyle(
                        it, R.raw.mapstyle)
                })
            Configuration.UI_MODE_NIGHT_NO ->
                googleMap.setMapStyle(context?.let {
                    MapStyleOptions.loadRawResourceStyle(
                        it, R.raw.standardmap)
                })
        }


        enableLocation()

        map.setOnMapLongClickListener {
            // Lat Lng del click en pantalla
//            Toast.makeText(context, "LAT: ${it.latitude}\nLNG: ${it.longitude}", Toast.LENGTH_SHORT).show()
            val action = MapFragmentDirections.actionMapFragmentToAddMarkerFragment(
                it.latitude.toFloat(),
                it.longitude.toFloat()
            )
            findNavController().navigate(action)
        }
        viewModel.markerListLD.observe(viewLifecycleOwner, {createMarkers(it)})
    }

    fun createMarkers(list: List<MyMarker>) {
        for (marker in list) {
            val coordinates = LatLng(marker.latitude.toDouble(), marker.longitude.toDouble())
            val myMarker = MarkerOptions().position(coordinates).title(marker.name)
            map.addMarker(MarkerOptions()
                .position(myMarker.position)
                .icon(activity?.let { bitmapDescriptorFromVector(it, R.drawable.ic_location_outline) })
                .title(myMarker.title))
        }
//        map.animateCamera(
//            CameraUpdateFactory.newLatLngZoom(coordinates, 18f),
//            5000, null)
    }

    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor? {
        val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
        vectorDrawable!!.setBounds(
            0,
            0,
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight
        )
        val bitmap = Bitmap.createBitmap(
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    private fun isLocationPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }

    @SuppressLint("MissingPermission")
    private fun enableLocation() {
        if (!::map.isInitialized) return
        if (isLocationPermissionGranted()) {
            map.isMyLocationEnabled = true
        } else {
            requestLocationPermission()
        }
    }

    private fun requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            Toast.makeText(
                requireContext(),
                "Ves a la pantalla de permisos de l’aplicació i habilita el de Geolocalització",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), Companion.REQUEST_CODE_LOCATION
            )
        }
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_CODE_LOCATION -> if (grantResults.isNotEmpty() &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                map.isMyLocationEnabled = true
            } else {
                Toast.makeText(
                    requireContext(), "Accepta els permisos de geolocalització",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        if (!::map.isInitialized) return
        if (!isLocationPermissionGranted()) {
            map.isMyLocationEnabled = false
            Toast.makeText(
                requireContext(), "Accepta els permisos de geolocalització",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onStop() {
        super.onStop()
        if (map.myLocation != null && map.isMyLocationEnabled) {
            viewModel.myLocation = LatLng(map.myLocation.latitude, map.myLocation.longitude)
        }
    }
}