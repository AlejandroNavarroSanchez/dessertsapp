package com.example.dessertsapp.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.dessertsapp.R
import com.example.dessertsapp.ui.viewmodel.MainViewModel
import com.google.android.gms.common.SignInButton
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class RegisterFragment : Fragment(R.layout.fragment_register) {
    private lateinit var registerBtn: Button
    private lateinit var googleSignInBtn: SignInButton
    private lateinit var topAppBar: MaterialToolbar
    private lateinit var mailInput: TextInputLayout
    private lateinit var pswInput: TextInputLayout
    private lateinit var confirmPswInput: TextInputLayout
    private val viewModel: MainViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Init variables
        registerBtn = view.findViewById(R.id.register_btn)
        googleSignInBtn = view.findViewById(R.id.google_sign_in_btn)
        topAppBar = activity!!.findViewById(R.id.topAppBar)
        mailInput = view.findViewById(R.id.email_edit_text)
        pswInput = view.findViewById(R.id.password_edit_text)
        confirmPswInput = view.findViewById(R.id.confirm_password_edit_text)

        topAppBar.visibility = View.GONE

        // === REGISTER BUTTON ===
        registerBtn.setOnClickListener {
            val email = mailInput.editText?.text.toString()
            val password = pswInput.editText?.text.toString()
            val confirmPassword = confirmPswInput.editText?.text.toString()

            if (password == confirmPassword) {

                registerUser(email, password)

            } else {
                Toast.makeText(context, "Passwords doesn't match", Toast.LENGTH_SHORT).show()
                Toast.makeText(context, "${pswInput.editText!!.text}", Toast.LENGTH_SHORT).show()
                Toast.makeText(context, "${confirmPswInput.editText!!.text}", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun registerUser(email: String, password: String) {
        if (email == "") {
            Toast.makeText(context, "Email can't be empty", Toast.LENGTH_SHORT).show()
        } else if (password.length < 8) {
            Toast.makeText(context, "Password must be 8 characters minimum.", Toast.LENGTH_SHORT).show()
        } else {
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        viewModel.populateMarkers()
                        //Firestore
                        Firebase.firestore.collection("users").document(email).set(
                            hashMapOf(
                                "name" to "userName",
                                "lastname" to "userLastName")
                        )

                        val action =
                            RegisterFragmentDirections.actionRegisterFragmentToMapFragment()
                        findNavController().navigate(action)
                    } else {
                        Toast.makeText(context, "Error al registrar l'usuari", Toast.LENGTH_SHORT)
                            .show()
                    }
                }
        }
    }
}