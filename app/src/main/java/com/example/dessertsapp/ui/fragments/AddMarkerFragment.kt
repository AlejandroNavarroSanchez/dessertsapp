package com.example.dessertsapp.ui.fragments

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.dessertsapp.R
import com.example.dessertsapp.model.MyMarker
import com.example.dessertsapp.ui.viewmodel.MainViewModel
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import java.io.ByteArrayOutputStream

class AddMarkerFragment : Fragment(R.layout.fragment_add_marker) {
    private lateinit var topAppBar: MaterialToolbar
    private lateinit var latInputTxt: TextInputLayout
    private lateinit var lngInputTxt: TextInputLayout
    private lateinit var imageView: ImageView
    private lateinit var addMarkerBtn: Button
    private lateinit var cameraBtn: Button
    private lateinit var galleryBtn: Button
    private lateinit var newMarkerName: TextInputLayout
    private lateinit var newMarkerDesc: TextInputLayout
    private val viewModel: MainViewModel by activityViewModels()

    companion object {
        const val CAMERA_PERMISSION_CODE = 1
        const val CAMERA_REQUEST_CODE = 2
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Init variables
        topAppBar = activity!!.findViewById(R.id.topAppBar)
        latInputTxt = view.findViewById(R.id.lat_coord)
        lngInputTxt = view.findViewById(R.id.lng_coord)
        newMarkerName = view.findViewById(R.id.new_marker_name)
        newMarkerDesc = view.findViewById(R.id.new_marker_desc)

        imageView = view.findViewById(R.id.marker_image)
        addMarkerBtn = view.findViewById(R.id.add_marker_btn)
        cameraBtn = view.findViewById(R.id.camera_btn)
        galleryBtn = view.findViewById(R.id.gallery_btn)

        topAppBar.title = "New marker"

        latInputTxt.editText?.setText(arguments?.getFloat("lat").toString())
        lngInputTxt.editText?.setText(arguments?.getFloat("lng").toString())

        //Add button
        addMarkerBtn.setOnClickListener {
            val marker = MyMarker(
                viewModel.counter,
                arguments?.getFloat("lat")!!,
                arguments?.getFloat("lng")!!,
                newMarkerName.editText?.text.toString(),
                newMarkerDesc.editText?.text.toString(),
                imageView.drawable
            )

            viewModel.markerList.add(marker)
            viewModel.counter++

            //Firebase add marker
            val email = FirebaseAuth.getInstance().currentUser?.email
            Firebase.firestore.collection("users").document(email!!)
                .collection("markers").document(marker.id.toString())
                .set(
                    hashMapOf(
                        "name" to marker.name,
                        "description" to marker.desc,
                        "coords" to GeoPoint(marker.latitude.toDouble(), marker.longitude.toDouble())
                    )
                )

            val baos = ByteArrayOutputStream()
            val bitmap = imageView.drawable.toBitmap()
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos)
            val data = baos.toByteArray()

            val storage = FirebaseStorage.getInstance().getReference("images/$email/${marker.id}")
            storage.putBytes(data)


            val action = AddMarkerFragmentDirections.actionAddMarkerFragmentToMarkerListFragment()
            findNavController().navigate(action)
        }

        cameraBtn.setOnClickListener {
            if (ContextCompat.checkSelfPermission(
                    it.context,
                    android.Manifest.permission.CAMERA
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(intent, CAMERA_REQUEST_CODE)
            } else {
                ActivityCompat.requestPermissions(
                    activity!!,
                    arrayOf(android.Manifest.permission.CAMERA),
                    CAMERA_PERMISSION_CODE
                )
            }

        }

        galleryBtn.setOnClickListener { } //TODO
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CAMERA_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(intent, CAMERA_REQUEST_CODE)
            } else {
                Toast.makeText(
                    context, "You just denied permissions for camera." +
                            "Go to settings and allow them.", Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CAMERA_REQUEST_CODE) {
                val thumbNail: Bitmap = data?.extras?.get("data") as Bitmap
                imageView.setImageBitmap(thumbNail)
            }
        }
    }
}