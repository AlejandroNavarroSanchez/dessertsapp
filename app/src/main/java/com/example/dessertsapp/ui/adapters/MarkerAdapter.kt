package com.example.dessertsapp.ui.adapters

import android.annotation.SuppressLint
import android.graphics.BitmapFactory
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.dessertsapp.R
import com.example.dessertsapp.model.MyMarker
import com.example.dessertsapp.ui.fragments.MarkerListFragmentDirections
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import java.io.File
import java.util.*

class MarkerAdapter(val markerList: MutableList<MyMarker>) : RecyclerView.Adapter<MarkerAdapter.ViewHolder>() {

    var myList = markerList

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.marker_layout, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        viewHolder.itemTitle.text = markerList[i].name
        viewHolder.itemDesc.text = markerList[i].desc
        viewHolder.itemImage.setImageDrawable(markerList[i].image)

        viewHolder.bindData(markerList[i])

        viewHolder.itemView.setOnClickListener {
            val directions = MarkerListFragmentDirections.actionMarkerListFragmentToMarkerDetail(markerList[i].id)
            Navigation.findNavController(it).navigate(directions)
        }
    }

    override fun getItemCount(): Int {
        return myList.size
    }

    fun deleteMarker(pos: Int) {
        val marker = markerList[pos]

        markerList.removeAt(pos)
        notifyItemRemoved(pos)

        val email = FirebaseAuth.getInstance().currentUser?.email
        Firebase.firestore
            .collection("users").document(email!!)
            .collection("markers").document(marker.id.toString())
            .delete()

        val storage = FirebaseStorage.getInstance().getReference("images/$email/${marker.id}")
        storage.delete()
    }


    @SuppressLint("ResourceAsColor")
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var itemTitle: TextView
        var itemDesc: TextView
        var itemImage: ImageView

        init {
            itemTitle = itemView.findViewById(R.id.title)
            itemDesc = itemView.findViewById(R.id.desc)
            itemImage = itemView.findViewById(R.id.marker_image)
        }

        fun bindData(marker: MyMarker) {
            itemTitle.text = marker.name
            itemDesc.text = marker.desc
            
            if (itemImage.drawable == null) {
                itemImage.setImageResource(R.drawable.ic_camera_outline)
            }

            Handler().postDelayed({
                // Gather Firestore Storage images
                val user = FirebaseAuth.getInstance().currentUser

                val storage = FirebaseStorage.getInstance().reference.child("images/${user?.email}/${marker.id}")
                val localFile = File.createTempFile("temp", "png")
                storage.getFile(localFile).addOnSuccessListener {
                    val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
                    itemImage.setImageBitmap(bitmap)
                }
            }, 1000)
        }
    }

}