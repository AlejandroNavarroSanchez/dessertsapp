package com.example.dessertsapp.model

import android.graphics.Bitmap
import android.graphics.drawable.Drawable

data class MyMarker (val id: Long, var latitude: Float, var longitude: Float, var name: String, var desc: String, var image: Drawable?)