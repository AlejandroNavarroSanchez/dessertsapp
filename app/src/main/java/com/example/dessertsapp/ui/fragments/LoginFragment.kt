package com.example.dessertsapp.ui.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.core.text.set
import androidx.core.text.toSpannable
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.dessertsapp.R
import com.example.dessertsapp.ui.viewmodel.MainViewModel
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider

enum class ProviderType {
    BASIC,
    GOOGLE
}

class LoginFragment : Fragment(R.layout.fragment_login) {

    private val GOOGLE_SIGN_IN = 100

    private lateinit var signInBtn: Button
    private lateinit var googleSignInBtn: SignInButton
    private lateinit var topAppBar: MaterialToolbar
    private lateinit var registerLink: TextView
    private lateinit var emailLoginInput: TextInputLayout
    private lateinit var passLoginInput: TextInputLayout
    private val viewModel: MainViewModel by activityViewModels()

    @SuppressLint("ResourceType")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Init variables
        signInBtn = view.findViewById(R.id.sign_in_btn)
        googleSignInBtn = view.findViewById(R.id.google_sign_in_btn)
        topAppBar = activity!!.findViewById(R.id.topAppBar)
        registerLink = view.findViewById(R.id.register_link)
        emailLoginInput = view.findViewById(R.id.email_edit_text)
        passLoginInput = view.findViewById(R.id.password_edit_text)

        topAppBar.visibility = View.GONE

        // Authomatic login if a user is already logged in
        var user = FirebaseAuth.getInstance().currentUser
        if (user != null) {
            viewModel.populateMarkers()
            findNavController().navigate(R.id.mapFragment)
        }

        // === REGISTER ===
        // Linked text
        val text = ("Create an account").toSpannable()
        // Set clickable span
        text[0..text.length] = object: ClickableSpan(){
            override fun onClick(view: View) {
                findNavController().navigate(R.id.registerFragment)
            }

            override fun updateDrawState(ds: TextPaint) {
                val nightModeFlags = context!!.resources.configuration.uiMode and
                        Configuration.UI_MODE_NIGHT_MASK
                when (nightModeFlags) {
                    Configuration.UI_MODE_NIGHT_YES -> ds.color = Color.parseColor("#ffb6ca")
                    Configuration.UI_MODE_NIGHT_NO -> ds.color = Color.parseColor("#786b94")
                }

                ds.isUnderlineText = true
            }
        }
        // Make the TextView text clickable for register link
        registerLink.movementMethod = LinkMovementMethod()
        registerLink.text = text

        // === SIGN IN BUTTON ===
        signInBtn.setOnClickListener {
            val email = emailLoginInput.editText?.text.toString()
            val password = passLoginInput.editText?.text.toString()
            logUser(email, password)
        }

        googleSignInBtn.setOnClickListener {

            // Config

            val googleConf = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()

            val googleClient = GoogleSignIn.getClient(activity!!, googleConf)
            googleClient.signOut()

            startActivityForResult(googleClient.signInIntent, GOOGLE_SIGN_IN)

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == GOOGLE_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)

            try {
                val account = task.getResult(ApiException::class.java)

                if (account != null) {

                    val credential = GoogleAuthProvider.getCredential(account.idToken, null)

                    FirebaseAuth.getInstance().signInWithCredential(credential)
                        .addOnCompleteListener {

                            if (it.isSuccessful) {
                                viewModel.populateMarkers()
                                val action =
                                    LoginFragmentDirections.actionLoginFragmentToMapFragment()
                                findNavController().navigate(action)
                            } else {
                                Toast.makeText(context, "Error al fer login", Toast.LENGTH_SHORT)
                                    .show()
                            }

                        }

                }
            }catch (e: ApiException) {
                Toast.makeText(context, "Error al fer login", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun logUser(email: String, password: String) {
        if (email == "") {
            Toast.makeText(context, "Email field must not be empty.", Toast.LENGTH_SHORT).show()
        }
        if (password == "") {
            Toast.makeText(context, "Password field must not be empty.", Toast.LENGTH_SHORT).show()

        } else if (password.length < 8) {
            Toast.makeText(context, "Password must have at least 8 characters.", Toast.LENGTH_SHORT).show()

        } else {
            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        viewModel.populateMarkers()
                        val action = LoginFragmentDirections.actionLoginFragmentToMapFragment()
                        findNavController().navigate(action)
                    } else {
                        Toast.makeText(context, "Error al fer login", Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }
}