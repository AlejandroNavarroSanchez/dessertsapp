package com.example.dessertsapp.ui.adapters

import android.app.AlertDialog
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView

class SwipeToDelete(var adapter: MarkerAdapter): ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        TODO("Not yet implemented")
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val pos = viewHolder.adapterPosition

        AlertDialog.Builder(viewHolder.itemView.context)
            .setCancelable(false)
            .setMessage("Are you sure you want to delete that marker?")
            .setPositiveButton("Yes") { _, _ ->
                adapter.deleteMarker(pos)
            }
            .setNegativeButton("No") { _, _ ->
                adapter.notifyItemChanged(pos)
            }
            .create()
            .show()
    }
}