package com.example.dessertsapp.ui.fragments

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.dessertsapp.R
import com.example.dessertsapp.ui.viewmodel.MainViewModel
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import java.io.ByteArrayOutputStream
import java.io.File

class MarkerDetail : Fragment(R.layout.fragment_marker_detail) {
    private val viewModel: MainViewModel by activityViewModels()
    private lateinit var image: ImageView
    private lateinit var cameraBtn: Button
    private lateinit var titleInput: TextInputLayout
    private lateinit var descInput: TextInputLayout
    private lateinit var confirmBtn: Button
    private lateinit var cancelBtn: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        image = view.findViewById(R.id.marker_detail_image)
        cameraBtn = view.findViewById(R.id.detail_camera_btn)
        titleInput = view.findViewById(R.id.marker_detail_titleInput)
        descInput = view.findViewById(R.id.marker_detail_descInput)
        confirmBtn = view.findViewById(R.id.confirm_btn)
        cancelBtn = view.findViewById(R.id.cancel_btn)

        loadArguments()

    }

    private fun loadArguments() {
        val markerID = arguments?.getLong("marker_id")
        val marker = viewModel.getMarkers().filter { it.id == markerID }[0]

        if (image.drawable == null) {
            image.setImageResource(R.drawable.ic_camera_outline)
        }

        // Gather Firestore Storage images
        val user = FirebaseAuth.getInstance().currentUser

        val storage = FirebaseStorage.getInstance().reference.child("images/${user?.email}/${marker.id}")
        val localFile = File.createTempFile("temp", "png")
        storage.getFile(localFile).addOnSuccessListener {
            val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
            image.setImageBitmap(bitmap)
        }

//        image.setImageDrawable(marker.image)
        titleInput.hint = marker.name
        titleInput.editText?.setText(marker.name)
        descInput.hint = marker.desc
        descInput.editText?.setText(marker.desc)

        cameraBtn.setOnClickListener {
            if (ContextCompat.checkSelfPermission(
                    it.context,
                    android.Manifest.permission.CAMERA
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(intent, AddMarkerFragment.CAMERA_REQUEST_CODE)
            } else {
                ActivityCompat.requestPermissions(
                    activity!!,
                    arrayOf(android.Manifest.permission.CAMERA),
                    AddMarkerFragment.CAMERA_PERMISSION_CODE
                )
            }

        }

        confirmBtn.setOnClickListener {
            marker.name = titleInput.editText?.text.toString()
            marker.desc = descInput.editText?.text.toString()
            marker.image = image.drawable

            // Saving new photo to Firebase Storage
            val baos = ByteArrayOutputStream()
            val bitmap = image.drawable.toBitmap()
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos)
            val data = baos.toByteArray()

            val email= FirebaseAuth.getInstance().currentUser?.email

            val storage = FirebaseStorage.getInstance().getReference("images/$email/${marker.id}")
            storage.putBytes(data).addOnSuccessListener{

                Firebase.firestore.collection("users").document(email!!)
                    .collection("markers").document(marker.id.toString())
                    .update(
                        mapOf(
                            "name" to marker.name,
                            "description" to marker.desc
                        )
                    )
            }

            val action = MarkerDetailDirections.actionMarkerDetailToMarkerListFragment()
            findNavController().navigate(action)
        }

        cancelBtn.setOnClickListener {
            val action = MarkerDetailDirections.actionMarkerDetailToMarkerListFragment()
            findNavController().navigate(action)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == AddMarkerFragment.CAMERA_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(intent, AddMarkerFragment.CAMERA_REQUEST_CODE)
            } else {
                Toast.makeText(
                    context, "You just denied permissions for camera." +
                            "Go to settings and allow them.", Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AddMarkerFragment.CAMERA_REQUEST_CODE) {
                val thumbNail: Bitmap = data?.extras?.get("data") as Bitmap
                image.setImageBitmap(thumbNail)
            }
        }
    }
}